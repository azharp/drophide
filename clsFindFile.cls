VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFindFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private Const MAX_PATH                 As Integer = 260
Private Const ERRBASE                  As Long = -2147218670
Private Const INVALID_HANDLE_VALUE     As Integer = -1
Private Type FILETIME
    dwLowDateTime                          As Long
    dwHighDateTime                         As Long
End Type
Private Type WIN32_FIND_DATA
    dwFileAttributes                       As Long
    ftCreationTime                         As FILETIME
    ftLastAccessTime                       As FILETIME
    ftLastWriteTime                        As FILETIME
    nFileSizeHigh                          As Long
    nFileSizeLow                           As Long
    dwReserved0                            As Long
    dwReserved1                            As Long
    cFileName                              As String * MAX_PATH
    cAlternate                             As String * 14
End Type
Private mstrPath                       As String
Private mobjParent                     As clsFindFile
Private mysnRoot                       As Boolean
Private ysnHasRunBefore                As Boolean
Private mobjChildren()                 As clsFindFile
Private mstrPathBuffer()               As String
Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, _
                                                                              lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, _
                                                                            lpFindFileData As WIN32_FIND_DATA) As Long

Private Sub Add(objDirectory As clsFindFile)

    Set mobjChildren(Count) = objDirectory
    ReDim Preserve mobjChildren(0 To Count + 1)

End Sub

Private Sub Class_Initialize()

    ReDim mobjChildren(0 To 0)

End Sub

Private Sub Class_Terminate()

Dim lngCount As Long

    On Error Resume Next
    lngCount = Count - 1
    Do While lngCount >= 0
        Set mobjChildren(lngCount) = Nothing
        lngCount = lngCount - 1
    Loop
    Erase mobjChildren
    Set mobjParent = Nothing
    On Error GoTo 0

End Sub

Public Sub Construct(ByRef objParent As clsFindFile, _
                     ByVal strPath As String, _
                     Optional ByVal ysnRoot As Boolean = False)

    If ysnHasRunBefore Then
        Err.Raise ERRBASE + 1, "clsFindFile Construct", "Construct method can only be used once."
    Else
        If ysnRoot And Not IsDirectory(strPath) Then
            Err.Raise ERRBASE + 2, "clsFindFile Construct", "Object cannot be the root if it is not a directory."
        End If
        If Not ((Not (objParent Is Nothing)) Xor ysnRoot) Then
            Err.Raise ERRBASE + 2, "clsFindFile Construct", "Object must be either root or have a parent."
        End If
        If Len(strPath) = 0 Then
            Err.Raise ERRBASE + 2, "clsFindFile Construct", "Path cannot be empty."
        End If
        If ysnRoot Then
            mysnRoot = True
        Else
            Set mobjParent = objParent
        End If
        mstrPath = strPath
        If IsDirectory(strPath) Then
            ReadDirectories
        End If
        ysnHasRunBefore = True
    End If

End Sub

Public Function Count() As Long
Attribute Count.VB_Description = "number ofsubentries if the entry is a directory, 0 otherwise"

    Count = UBound(mobjChildren)

End Function

Private Sub CreateDirectoryEntries()

Dim lngCount As Long
Dim objDir   As clsFindFile

    lngCount = UBound(mstrPathBuffer) - 1
    Do While lngCount >= 0
        Set objDir = New clsFindFile
        objDir.Construct Me, mstrPathBuffer(lngCount)
        Add objDir
        lngCount = lngCount - 1
    Loop
    Erase mstrPathBuffer
    Set objDir = Nothing

End Sub

Private Function IsDirectory(ByVal strFile As String) As Boolean

    On Error Resume Next
    IsDirectory = (GetAttr(strFile) And VbFileAttribute.vbDirectory) > 0

End Function

Public Function Item(ByVal lngWhich As Long) As clsFindFile
Attribute Item.VB_UserMemId = 0
Attribute Item.VB_MemberFlags = "40"

    If (lngWhich > Count) Or (lngWhich < 1) Then
        Err.Raise ERRBASE + 3, "clsFindFile item", "Index out of bounds."
    End If
    Set Item = mobjChildren(lngWhich - 1)

End Function

Public Function Path() As String

    Path = mstrPath

End Function

Private Sub ReadDirectories()

    ReadPathNames
    CreateDirectoryEntries

End Sub

Private Sub ReadPathNames()

Dim strPath         As String
Dim lngSearchHandle As Long
Dim typWFD          As WIN32_FIND_DATA

    ReDim mstrPathBuffer(0 To 0)
    strPath = mstrPath & "\*"
    lngSearchHandle = FindFirstFile(strPath, typWFD)
    If lngSearchHandle <> INVALID_HANDLE_VALUE Then
        strPath = StripNulls(typWFD.cFileName)
        Do While Len(strPath) > 0
            If strPath <> "." Then
                If strPath <> ".." Then
                    mstrPathBuffer(UBound(mstrPathBuffer)) = mstrPath & "\" & strPath
                    ReDim Preserve mstrPathBuffer(0 To UBound(mstrPathBuffer) + 1)
                End If
            End If
            If FindNextFile(lngSearchHandle, typWFD) Then
                strPath = StripNulls(typWFD.cFileName)
            Else
                strPath = vbNullString
            End If
        Loop
        FindClose lngSearchHandle
    End If

End Sub

Public Sub SetAttributes(ByVal lngAttr As VbFileAttribute)

Dim lngCount As Long

    On Error Resume Next
    'If UCase$(GetFileName(mstrPath)) <> "DESKTOP.INI" Then
        SetAttr mstrPath, lngAttr
    'End If
    lngCount = Count - 1
    Do While lngCount >= 0
        mobjChildren(lngCount).SetAttributes lngAttr
        lngCount = lngCount - 1
    Loop

End Sub

Private Function StripNulls(ByVal strOriginal As String) As String

    If (InStr(strOriginal, vbNullChar) > 0) Then
        strOriginal = Left$(strOriginal, InStr(1, strOriginal, vbNullChar, vbTextCompare) - 1)
    End If
    StripNulls = strOriginal

End Function


