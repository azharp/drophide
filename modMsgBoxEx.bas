Attribute VB_Name = "modMsgBoxEx"
Option Explicit
Private Const WH_CBT                   As Long = 5
Private Const HCBT_ACTIVATE            As Long = 5
Private Const HWND_TOP                 As Long = 0
Private Const SWP_NOSIZE               As Long = &H1
Private Const SWP_NOZORDER             As Long = &H4
Private Const SWP_NOACTIVATE           As Long = &H10
Private Const SWVB_DEFAULT             As Long = &HFFFFFFFF
Private Const SWVB_CAPTION_DEFAULT     As String = "SWVB_DEFAULT_TO_APP_TITLE"
Private Type RECT
    Left                                   As Long
    Top                                    As Long
    Right                                  As Long
    Bottom                                 As Long
End Type
Private mhWnd                          As Long
Private msPrompt                       As String
Private mlHook                         As Long
Private mlLeft                         As Long
Private mlTop                          As Long
Private mlOptions                      As Long
Private msButtonText()                 As String
Private Declare Function CallNextHookEx Lib "user32" (ByVal hHook As Long, _
                                                      ByVal nCode As Long, _
                                                      ByVal wParam As Long, _
                                                      ByVal lParam As Long) As Long
Private Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" (ByVal ParenthWnd As Long, _
                                                                          ByVal ChildhWnd As Long, _
                                                                          ByVal ClassName As String, _
                                                                          ByVal Caption As String) As Long
Private Declare Function GetClassName Lib "user32" Alias "GetClassNameA" (ByVal hwnd As Long, _
                                                                          ByVal lpClassName As String, _
                                                                          ByVal nMaxCount As Long) As Long
Private Declare Function GetCurrentThreadId Lib "kernel32" () As Long
Private Declare Function GetParent Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, _
                                                     lpRect As RECT) As Long
Private Declare Function SetWindowsHookEx Lib "user32" Alias "SetWindowsHookExA" (ByVal idHook As Long, _
                                                                                  ByVal lpfn As Long, _
                                                                                  ByVal hmod As Long, _
                                                                                  ByVal dwThreadId As Long) As Long
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, _
                                                    ByVal hWndInsertAfter As Long, _
                                                    ByVal X As Long, _
                                                    ByVal Y As Long, _
                                                    ByVal cx As Long, _
                                                    ByVal cy As Long, _
                                                    ByVal wFlags As Long) As Long
Private Declare Function SetWindowText Lib "user32.dll" Alias "SetWindowTextA" (ByVal hwnd As Long, _
                                                                                ByVal lpString As String) As Long
Private Declare Function UnhookWindowsHookEx Lib "user32" (ByVal hHook As Long) As Long

Private Sub ButtonCaption(ByVal wParam As Long, _
                          ByVal TxtBut As String, _
                          ByVal StrCap As String)

    If LenB(TxtBut) Then
        SetWindowText FindWindowEx(wParam, 0&, "Button", StrCap), TxtBut
    End If

End Sub

Public Function MsgBoxEx(ByVal Prompt As String, _
                         Optional ByVal Options As VbMsgBoxStyle = vbOKOnly, _
                         Optional ByVal Title As String = SWVB_CAPTION_DEFAULT, _
                         Optional ByVal HelpFile As String, _
                         Optional ByVal Context As Long, _
                         Optional ByVal lngLeft As Long = SWVB_DEFAULT, _
                         Optional ByVal lngTop As Long = SWVB_DEFAULT, _
                         Optional ByVal ButtonText As String = "||||") As VbMsgBoxResult

Dim hInst     As Long

Dim lThreadID As Long
    If ButtonText <> vbNullString Then
        msButtonText = Split(ButtonText, "|")
    End If
    hInst = App.hInstance
    lThreadID = GetCurrentThreadId()
    mlHook = SetWindowsHookEx(WH_CBT, AddressOf MsgBoxHook, hInst, lThreadID)
    mlLeft = lngLeft
    mlTop = lngTop
    mlOptions = Options
    msPrompt = Prompt
    If Title = SWVB_CAPTION_DEFAULT Then
        Title = App.Title
    End If
    MsgBoxEx = MsgBox(msPrompt, mlOptions, Title, HelpFile, Context)

End Function

Private Function MsgBoxHook(ByVal nCode As Long, _
                            ByVal wParam As Long, _
                            ByVal lParam As Long) As Long

Dim lHgt     As Long

Dim lWid     As Long
Dim lSize    As Long
Dim rcMsgBox As RECT
Dim sBuff    As String
Dim lPosX    As Long
Dim lPosY    As Long
Dim X        As Long
Dim Y        As Long
    MsgBoxHook = CallNextHookEx(mlHook, nCode, wParam, lParam)
    If nCode = HCBT_ACTIVATE Then
        sBuff = Space$(32)
        lSize = GetClassName(wParam, sBuff, 32)
        If Left$(sBuff, lSize) <> "#32770" Then
            Exit Function
        End If
        mhWnd = FindWindowEx(0&, 0&, "#32770", vbNullString)
        GetWindowRect wParam, rcMsgBox
        lHgt = (rcMsgBox.Bottom - rcMsgBox.Top) / 2
        lWid = (rcMsgBox.Right - rcMsgBox.Left) / 2
        GetWindowRect GetParent(wParam), rcMsgBox
        lPosY = rcMsgBox.Top + (rcMsgBox.Bottom - rcMsgBox.Top) / 2
        lPosX = rcMsgBox.Left + (rcMsgBox.Right - rcMsgBox.Left) / 2
        If mlLeft = SWVB_DEFAULT Then
            X = lPosX - lWid
        Else
            X = mlLeft
        End If
        If mlTop = SWVB_DEFAULT Then
            Y = lPosY - lHgt
        Else
            Y = mlTop
        End If
        FindWindowEx wParam, 0&, "Static", msPrompt
        SetWindowPos wParam, HWND_TOP, X, Y, 0&, 0&, SWP_NOZORDER + SWP_NOACTIVATE + SWP_NOSIZE
        SetButtonCaptions wParam
ErrHandler:
        UnhookWindowsHookEx mlHook
    End If

End Function

Private Sub SetButtonCaptions(ByVal wParam As Long)

Dim hwnd  As Long
Dim sText As String

    If (mlOptions And vbRetryCancel) = vbRetryCancel Then
        ButtonCaption wParam, msButtonText(0), "&Retry"
        ButtonCaption wParam, msButtonText(1), "Cancel"
    ElseIf (mlOptions And vbYesNo) = vbYesNo Then
        ButtonCaption wParam, msButtonText(0), "&Yes"
        ButtonCaption wParam, msButtonText(1), "&No"
    ElseIf (mlOptions And vbYesNoCancel) = vbYesNoCancel Then
        ButtonCaption wParam, msButtonText(0), "&Yes"
        ButtonCaption wParam, msButtonText(1), "&No"
        ButtonCaption wParam, msButtonText(2), "Cancel"
    ElseIf (mlOptions And vbAbortRetryIgnore) = vbAbortRetryIgnore Then
        ButtonCaption wParam, msButtonText(0), "&Abort"
        ButtonCaption wParam, msButtonText(1), "&Retry"
        ButtonCaption wParam, msButtonText(2), "&Ignore"
    ElseIf (mlOptions And vbOKCancel) = vbOKCancel Then
        ButtonCaption wParam, msButtonText(0), "&Ok"
        ButtonCaption wParam, msButtonText(1), "Cancel"
    ElseIf (mlOptions And vbOKOnly) = vbOKOnly Then
        ButtonCaption wParam, msButtonText(0), "Ok"
    End If
    If (mlOptions And vbMsgBoxHelpButton) = vbMsgBoxHelpButton Then
        If msButtonText(3) <> vbNullString Then
            sText = "Help"
            hwnd = FindWindowEx(wParam, 0&, "Button", sText)
            SetWindowText hwnd, msButtonText(3)
        End If
    End If

End Sub


