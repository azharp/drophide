Attribute VB_Name = "modMain"
Option Explicit
Private Data()           As String
Private InputPath        As String
Private TotalItems       As String
Private CountedFolder    As String
Private CountedFolder2   As Integer

Private Sub CheckFile()

Dim MyApps As String
Dim SendTo As String

    On Error GoTo ErrorFunct
    MyApps = App.Path & "\" & App.EXEName & ".exe"
    MyApps = StripSlash(MyApps)
    SendTo = GetSpecialfolder(&H9) & "\Show or Hide File & Folder.exe"
    If IsFileExist(SendTo) Then
        Select Case MsgBoxEx("Do you want to reinstall/uninstall DropHide?", vbExclamation + vbYesNoCancel, , , , , , "Reinstall!|Uninstall!|Cancel")
        Case vbYes
            CopyFile MyApps, SendTo, False
            MsgBox "DropHide has been reinstalled, Now it's ready to use!", vbInformation
        Case vbNo
            Kill GetSpecialfolder(&H9) & "\Show or Hide File & Folder.exe"
            MsgBox "DropHide has been uninstalled!", vbInformation
        End Select
    Else
        If MsgBoxEx("Do you want to install DropHide to ""Send To"" context menu?", vbQuestion + vbYesNo, , , , , , "Install!|Cancel") = vbYes Then
            CopyFile MyApps, SendTo, False
            MsgBox "DropHide installation has completed!" & vbNewLine & _
       "Now you can use it from ""Send To"" context menu." & vbNewLine & _
       vbNewLine & _
       "NOTE : To uninstall just run the this file.", vbInformation
        End If
    End If
    End

Exit Sub

ErrorFunct:
    ErrorHandler Err, "File Check"
    Resume Next

End Sub

Private Function FileInfo() As String

Dim i       As Integer
Dim Files   As Integer
Dim Folders As Integer
Dim Hide    As Integer
Dim Normal  As Integer

    On Error Resume Next
    For i = 0 To UBound(Data)
        Select Case GetAttr(Data(i))
        Case 0, 4, 16, 17, 20, 32, 8192, 8208, 8209, 8212, 8224, 8225
            Hide = Hide + 1
        Case Else
            Normal = Normal + 1
        End Select
        If IsDirectory(Data(i)) Then
            Folders = Folders + 1
        Else
            Files = Files + 1
        End If
    Next i
    If Hide > Normal Then
        FileInfo = "Hide"
    Else
        FileInfo = "Normal"
    End If
    FileInfo = Files & " files and " & Folders & " folders:::" & FileInfo
    CountedFolder = Folders
    CountedFolder2 = Folders
    On Error GoTo 0

End Function

Private Sub HidePath()

Dim i       As Integer
Dim bolMsg  As Boolean
Dim strMsg  As String
Dim EstTime As Long

    On Error Resume Next
    Select Case MsgBoxEx("Dropped items : " & TotalItems & vbNewLine & _
                   vbNewLine & _
                   "What are you going to do with these items?", vbQuestion + vbYesNoCancel, , , , , , "SuperHidden!|Hidden|Cancel")
    Case vbYes
        If CountedFolder2 > 0 Then
            If MsgBox("Do you want to superhide all files in subfolders?", vbYesNo + vbQuestion) = vbYes Then
                bolMsg = True
            End If
        End If
        EstTime = GetTickCount
        For i = 0 To UBound(Data)
            SetAttr Data(i), vbHidden + vbSystem
            If bolMsg And IsDirectory(Data(i)) Then
                With New clsFindFile
                    .Construct Nothing, Data(i), True
                    .SetAttributes vbHidden + vbSystem
                End With
            End If
        Next i
        EstTime = GetTickCount - EstTime
        If bolMsg Then
            strMsg = "Dropped items """ & TotalItems & """ is now superhidden!" & vbNewLine & _
                     vbNewLine & _
                     "NOTE : Including all files in " & CountedFolder & " subfolders!"
        Else
            strMsg = "Dropped items """ & TotalItems & """ is now superhidden!"
        End If
        MsgBox strMsg, vbInformation, App.Title & " - Done in " & EstTime / 1000 & " seconds"
    Case vbNo
        If CountedFolder2 > 0 Then
            If MsgBox("Do you want to hide all files in subfolders?", vbYesNo + vbQuestion) = vbYes Then
                bolMsg = True
            End If
        End If
        EstTime = GetTickCount
        For i = 0 To UBound(Data)
            SetAttr Data(i), vbHidden
            If bolMsg And IsDirectory(Data(i)) Then
                With New clsFindFile
                    .Construct Nothing, Data(i), True
                    .SetAttributes vbHidden
                End With
            End If
        Next i
        EstTime = GetTickCount - EstTime
        If bolMsg Then
            strMsg = "Dropped items """ & TotalItems & """ is now hidden!" & vbNewLine & _
                     vbNewLine & _
                     "NOTE : Including all files in " & CountedFolder & " subfolders!"
        Else
            strMsg = "Dropped items """ & TotalItems & """ is now hidden!"
        End If
        MsgBox strMsg, vbInformation, App.Title & " - Done in " & EstTime / 1000 & " seconds"
    End Select
    End
    On Error GoTo 0

End Sub

Private Sub Main()

Dim Info() As String

    On Error GoTo ErrorFunct
    CheckInstance
    InitCommonControls
    InputPath = Command$
    If LenB(InputPath) = 0 Then
        MsgBox "Please drag 'n drop files to this application to make it hidden." & vbNewLine & _
       vbNewLine & _
       "   ---You can select multiple files and drop them!---", vbInformation
        CheckFile
    Else
        Data = ParseCommandLine(InputPath)
        Info = Split(FileInfo, ":::")
        If UBound(Data) = 0 Then
            TotalItems = ShortFilename(GetFileName(Data(0)))
            CountedFolder = ShortFilename(GetFileName(Data(0)))
        Else
            TotalItems = Info(0)
        End If
        If Right$(Data(0), 2) = ":\" Then
            MsgBox "Cannot change attributes of Drive!", vbCritical
        Else
            Select Case Info(1)
            Case "Hide"
                HidePath
            Case Else
                Normalize
            End Select
        End If
    End If
    End

Exit Sub

ErrorFunct:
    ErrorHandler Err, "Main Procedure"
    Resume Next

End Sub

Private Sub Normalize()

Dim i       As Integer
Dim bolMsg  As Boolean
Dim strMsg  As String
Dim EstTime As Long

    On Error Resume Next
    Select Case MsgBoxEx("Dropped items : " & TotalItems & vbNewLine & _
                   vbNewLine & _
                   "Item is not in normal attributes, normalize it?", vbQuestion + vbYesNoCancel, , , , , , "Normalize!|Hide!|Cancel")
    Case vbYes
        If CountedFolder2 > 0 Then
            If MsgBox("Do you want to normalize all files in subfolders?", vbYesNo + vbQuestion) = vbYes Then
                bolMsg = True
            End If
        End If
        EstTime = GetTickCount
        For i = 0 To UBound(Data)
            SetAttr Data(i), vbNormal
            If bolMsg And IsDirectory(Data(i)) Then
                With New clsFindFile
                    .Construct Nothing, Data(i), True
                    .SetAttributes vbNormal
                End With
            End If
        Next i
        EstTime = GetTickCount - EstTime
        If bolMsg Then
            strMsg = "Dropped items """ & TotalItems & """ is now normal!" & vbNewLine & _
                     vbNewLine & _
                     "NOTE : Including all files in " & CountedFolder & " subfolders!"
        Else
            strMsg = "Dropped items """ & TotalItems & """ is now normal!"
        End If
        MsgBox strMsg, vbInformation, App.Title & " - Done in " & EstTime / 1000 & " seconds"
    Case vbNo
        HidePath
    End Select
    End
    On Error GoTo 0

End Sub

'Private Sub ProcessData(FileAttributes As VbFileAttribute)
'
'Dim i As Integer
'
'    For i = 0 To UBound(Data)
'        SetAttr Data(i), FileAttributes
'        If bolMsg And Not IsFileExist(Data(i)) Then
'            With New clsFindFile
'                .Construct Nothing, Data(i), True
'                .SetAttributes FileAttributes
'            End With
'        End If
'    Next i
'
'End Sub


