Attribute VB_Name = "modFunction"
Option Explicit

Public Sub CheckInstance()

Dim sTitle  As String
Dim lHandle As Long

    sTitle = App.Title
    App.Title = sTitle & " - New"
    lHandle = FindWindow("#32770", sTitle)
    App.Title = sTitle
    If lHandle Then
        ShowWindow lHandle, 1
        SetForegroundWindow lHandle
        End
    End If

End Sub

Public Sub ErrorHandler(ByVal sError As ErrObject, _
                        ByVal ProcedureName As String)

    MsgBox "An error has occurred please send complete error message to Author!" & vbNewLine & _
       vbNewLine & _
       "Procedure Name : " & ProcedureName & vbNewLine & _
       vbNewLine & _
       "Error Number      : " & Err.Number & vbNewLine & _
       "Error Description : " & sError.Description, vbCritical

End Sub

Public Function GetFileName(ByVal Fullpath As String) As String

Dim NullPos As Long

    PathStripPath Fullpath
    NullPos = InStr(1, Fullpath, vbNullChar)
    If NullPos > 0 Then
        GetFileName = Trim$(Left$(Fullpath, NullPos - 1))
    Else
        GetFileName = Trim$(Fullpath)
    End If

End Function

Public Function GetSpecialfolder(ByVal CSIDL As Long) As String

Dim Path As String
Dim r    As Long
Dim IDL  As ITEMIDLIST

    r = SHGetSpecialFolderLocation(100, CSIDL, IDL)
    If r = NOERROR Then
        Path = Space$(512)
        r = SHGetPathFromIDList(ByVal IDL.mkid.cb, ByVal Path)
        GetSpecialfolder = Left$(Path, InStr(Path, vbNullChar) - 1)
    Else
        GetSpecialfolder = vbNullString
    End If

End Function

Public Function IsDirectory(ByVal Path As String) As Boolean

    On Error GoTo Hell
    IsDirectory = PathIsDirectory(Path)

Exit Function

Hell:
    IsDirectory = False

End Function

Public Function IsFileExist(ByVal Path As String) As Boolean

    On Error GoTo Hell
    IsFileExist = PathFileExists(Path)

Exit Function

Hell:
    IsFileExist = False

End Function

Public Function ParseCommandLine(ByVal cmdLine As String) As String()

Dim i                As Integer
Dim inQuote          As Boolean
Dim curParam         As String
Dim returnArray()    As String
Dim curChar          As String
Dim arrayInitialized As Boolean

    If Not LenB(Trim$(cmdLine)) = 0 Then
        For i = 1 To Len(cmdLine)
            curChar = Mid$(cmdLine, i, 1)
            If inQuote Then
                If curChar = """" Then
                    inQuote = False
                Else
                    curParam = curParam & curChar
                End If
            ElseIf curChar = " " Then
                If LenB(curParam) Then
                    If arrayInitialized Then
                        ReDim Preserve returnArray(UBound(returnArray) + 1)
                    Else
                        ReDim returnArray(0) As String
                        arrayInitialized = True
                    End If
                    returnArray(UBound(returnArray)) = curParam
                    curParam = vbNullString
                End If
            Else
                If curChar = """" Then
                    inQuote = True
                Else
                    curParam = curParam & curChar
                End If
            End If
        Next i
        If LenB(curParam) Then
            If arrayInitialized Then
                ReDim Preserve returnArray(UBound(returnArray) + 1)
            Else
                ReDim returnArray(0) As String
                arrayInitialized = True
            End If
            returnArray(UBound(returnArray)) = curParam
            curParam = vbNullString
        End If
        ParseCommandLine = returnArray
    End If

End Function

Public Function ShortFilename(ByVal Filename As String) As String

    If Len(Filename) >= 40 Then
        If InStr(1, Filename, ".", vbTextCompare) Then
            ShortFilename = Left$(Filename, 35) & "~1" & Mid$(Filename, InStr(1, Filename, ".", vbTextCompare))
        Else
            ShortFilename = Left$(Filename, 38) & "~1"
        End If
    Else
        ShortFilename = GetFileName(Filename)
    End If

End Function

Public Function StripSlash(ByVal Fullpath As String) As String

    StripSlash = Replace$(Fullpath, "\\", "\")

End Function
