
+=============================================+
|               DropHide v1.5                 |
| Copyright (c) 2007 Just Script Kidding Team |
+=============================================+


DropHide adalah sebuah program sederhana yang berfungsi untuk
menyembunyikan/menampilkan file, dengan cara mengubah attribut
file yang bersangkutan. DropHide juga dapat mengubah attribut
file ke Super Hidden (System files).


File yang terdapat didalam paket :
==================================

1. DropHide.exe ---> Program utama DropHide.
2. Readme.txt ---> File yang sedang anda baca sekarang.


Cara install/uninstall DropHide :
===================================

1. Jalankan program DropHide.exe, klik Ok dan pada dialog
   berikutnya tekan Install!.
2. DropHide telah terinstall dan siap untuk digunakan.
3. Untuk menguninstall DropHide, jalankan program lagi dan
   ikuti petunjuk selanjutnya.


Cara penggunaan DropHide :
==========================

1. Untuk menggunakan DropHide, klik kanan file yang diinginkan
   lalu pilih Send To --> Show or Hide File & Folder.
2. Apabila file hidden maka akan ditampilkan mode menormalkan
   file.
2. Pilih metode yang diinginkan.
3. Klik yes untuk menyembunyikan/menampikan seluruh file 
   yang terdapat didalam subfolder yang anda pilih.


Keterbatasan pada DropHide :
============================

Karena adanya keterbatasan pada panjang karakter dari commandline 
yang diberikan Windows, maka ketika memilih file yang sangat 
banyak atau memiliki alamat/nama yang sangat panjang akan muncul
pesan error. Untuk solusinya, kurangi jumlah file yang dipilih 
atau pindahkan file ke alamat yang lebih pendek (contoh D:\TEMP)
lalu coba lagi.


Program History :
=================


Version 1.5.3
-------------

- Penggantian compressor program (FSG >>> UPX)
  karena terdeteksi sebagai virus oleh beberapa antivirus.

Version 1.5.2
-------------

- Perbaikan beberapa minor bug.
- Optimasi kode.

Version 1.5
-----------

- Penambahan fungsi show/hide file didalam subfolder.

Version 1.0
-----------

- Rilis awal DropHide.


Info tambahan :
===============

Untuk versi terbaru & program lainnya silakan
download di : 

gutz.4shared.com
----------------

Untuk bug-report silakan kirim ke e-mail :

azharpratama@gmail.com
----------------------


Our motto:
==========

Just Script Kidding Team --- "We don't understand but it works!"


END OF README FILE.....